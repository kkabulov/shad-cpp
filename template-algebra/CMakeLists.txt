cmake_minimum_required(VERSION 2.8)
project(template-algebra)

if (TEST_SOLUTION)
  include_directories(../private/template-algebra)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)
  
endif()

add_gtest(test_matrix test.cpp)
