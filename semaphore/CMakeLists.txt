cmake_minimum_required(VERSION 2.8)
project(semaphore)

if (TEST_SOLUTION)
    include_directories(../private/semaphore/wake_all)
endif()

include(../common.cmake)

add_gtest(test_semaphore test.cpp)
